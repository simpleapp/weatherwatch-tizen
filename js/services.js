'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('SimpleWeather.services', []).
  service('WeatherService', function($http,$q,$rootScope){


  	var async_return = function (obj) {
  	  var deferred = $q.defer();
  	 
  	  setTimeout(function() {
  	    // since this fn executes async in a future turn of the event loop, we need to wrap
  	    // our code into an $apply call so that the model changes are properly observed.
  	    $rootScope.$apply(function() {
  	        deferred.resolve(obj);
  	    });
  	  }, 100);
  	 
  	  return deferred.promise;
  	}


  	this.getByLatLon = function (latitude,longitude, date)
  	{
  		var query_date =  Math.round(Date.now()/1000);
  		var cache_key = Math.round(query_date); //cache key is date by hour
  		console.log("cache_key : "+cache_key);
  		console.log("localStorage : "+localStorage);
  		var cached_result = localStorage.getItem(cache_key);
  		if ( cached_result != null)
  		{
  			console.log("cache hit");
  			return async_return(JSON.parse(cached_result));
  		}

  		console.log("cache miss");
  		return $http.get('http://simpleapp.fr/proxy/forecast_proxy.php?url='+latitude+','+longitude+','+date+'?units=si').then(function(result){
  			console.log("request success");
  			localStorage.setItem(cache_key,JSON.stringify(result));
  			return result;

  		}, function(error){
  			console.log("request error : ");
  			console.log(error);
  			return $q.reject(error);
  		});
  	};

  });
