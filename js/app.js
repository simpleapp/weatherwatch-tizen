'use strict';


// Declare app level module which depends on filters, and services
var SimpleWeather = angular.module('SimpleWeather', ['SimpleWeather.filters', 'SimpleWeather.services', 'SimpleWeather.directives']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/weather', {templateUrl: 'partials/weather.html', controller: 'WeatherCtrl'});
    $routeProvider.otherwise({redirectTo: '/weather'});
  }]);



/* Access testing.
var handler = function(requestProgress)
{
	console.log("handler : ");
	console.log(requestProgress);
	var myRequestState = requestProgress.target.readyState;
	console.log("request.responseText : "+requestProgress.target.responseText);
}
var testRequest = new XMLHttpRequest();
testRequest.open("GET", "http://simpleapp.fr/proxy/forecast_proxy.php?url=48.8565956,2.4002501,1369346400?units=si");
testRequest.onreadystatechange = handler;
console.log("request send");
testRequest.send();
*/
