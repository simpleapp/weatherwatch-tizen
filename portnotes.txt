PLAN : 
We want to move from using 2 cadrans to only 1 , and a bottom menu showing "AM / PM" that will switch the data beeing displayed.
We will also need to use smaller cadrans.
We also will add an "exit" button to kill the app.


STEP 0 : 
copied / paste code into folders. 
replaced index.html
renamed /img to /images in app.css and weather.css
renamed /lib to /js/lib for angular.js librairie in index.html
added * permission in config.xml for URLs
authorized app in the settings app to access localization ON THE DEVICE ITSELF
added a raw XmlHttpRequest in app.js to check for permission for debugging

STEP 1 : 
In tizenIDE create new web app using "Navigation" Template, look at the structure of a jquery mobile + tizen webapp
Here, we'll do : use angular-js for the navigation, and jquerymobile + tizen extensions for the styling only (the opposite would probably have been harder, as in our case, the number of navigation items is dynamic).

- copy the "script" section in index.html to weatherapp index.html  ==> Since angular includes a "mini jquery", and is able to use the "real jquery" if provided, we need to include jquery.js first, then angular.js at the end.

- copy the tizen-web-ui-fw directory
- style.css is empty. ignore.
- copy the following at the bottom of weather.html (from index.html in the nav bar) . We only copy this since we're not using jquery mobile for page change :

<div data-role="footer" data-id="foo1">
   <div data-role="controlgroup" data-type="horizontal">
       <a data-role="button" href="index.html">Playlist</a>
       <a data-role="button" href="section1.html">Music</a>
       <a data-role="button" href="section2.html">Artist</a>
   </div><!-- /navbar -->
</div><!-- /footer -->

==> The footer is displayed, but it isn't styled. That's because jquery mobile starts styling after a special "page create event" was fired. Since we create the widget in a template, that event isn't fired by angular. We need to compensate that, by manually calling the "jqueryMobile element.create()" (in a deferred way ) using this call : http://jquerymobile.com/demos/1.0b2/#/demos/1.0b2/docs/pages/page-scripting.html

==> In angular, DOM related functionnalities are performed in a directive (in directives.js) : 
angular.module('SimpleWeather.directives', []).directive('applyJqMobile', function() {
    return function($scope, el) {
    	setTimeout(function(){
            $scope.$on('$viewContentLoaded', el.trigger("create"))},1);
    }
});

and then, simply add : 
<div data-role="footer" data-id="foo1" apply-jq-mobile>


STEP 2 : 
We now start binding the jquery mobile widgets to angular controller to modify the behavior. We'll add more skin in the way.

- We'll want a two level navbar : one for AM/PM , one for Exit. So let's use two controlgroup. Change the footer to that : 
<div data-role="footer" data-id="foo1" apply-jq-mobile>
   <div data-role="controlgroup" data-type="horizontal">
	   <a data-role="button" ng-click="onSelectAM()">AM</a>
	   <a data-role="button" ng-click="onSelectPM()">PM</a>
   </div><!-- /navbar -->
   
   <div data-role="controlgroup" data-type="horizontal">
	   <a data-role="button" data-icon="back" ng-click="onSelectExit()" style="float:right;width:50px;height:50px;"></a>
   </div><!-- /navbar -->
</div><!-- /footer -->